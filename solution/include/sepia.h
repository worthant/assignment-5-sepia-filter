#ifndef SEPIA_H
#define SEPIA_H

#include "image.h"

typedef void (*sepia_filter_func)(struct image*);

void sepia_c(struct image* img);
void sepia_sse(struct image* img);

#endif // SEPIA_H
