#ifndef PROCESSING_H
#define PROCESSING_H

void process_image(const char* source, const char* destination, const char* filter_type);

#endif // PROCESSING_H
