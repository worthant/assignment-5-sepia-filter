#include "sepia.h"

extern void sepia_asm(struct pixel* pixel);

static inline unsigned char sat(uint64_t x) {
    return x < 256 ? x : 255;
}

static void filter_pixel(struct pixel* const pixel) {
    static const float c[3][3] = {
            { .131f, .543f, .272f },
            { .168f, .686f, .349f },
            { .189f, .769f, .393f }
    };

    struct pixel const old = *pixel;
    pixel->b = sat(old.b * c[0][0] + old.g * c[0][1] + old.r * c[0][2]);
    pixel->g = sat(old.b * c[1][0] + old.g * c[1][1] + old.r * c[1][2]);
    pixel->r = sat(old.b * c[2][0] + old.g * c[2][1] + old.r * c[2][2]);
}

void sepia_c(struct image* img) {
    for (uint32_t y = 0; y < img->height; y++) {
        for (uint32_t x = 0; x < img->width; x++) {
            struct pixel* pixel = img->data + y * img->width + x;
            filter_pixel(pixel);
        }
    }
}

void sepia_sse(struct image* img) {
    for (uint32_t y = 0; y < img->height; y++) {
        for (uint32_t x = 0; x < img->width; x++) {
            struct pixel* pixel = img->data + y * img->width + x;
            sepia_asm(pixel);
        }
    }
}

