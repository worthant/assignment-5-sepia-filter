#include "processing.h"
#include <stdio.h>

int main(int argc, char* argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image> <filter-type>\n", argv[0]);
        return 1;
    }

    process_image(argv[1], argv[2], argv[3]);

    return 0;
}
