%define COLOR_SIZE 1

section .rodata
; Define constants for the color transformation matrix
red_factors:   dd 0.131, 0.168, 0.189, 0
green_factors: dd 0.534, 0.686, 0.769, 0
blue_factors:  dd 0.272, 0.349, 0.393, 0

section .text
global sepia_asm

; sepia_asm - Apply sepia filter to a pixel
; Parameters:
;   rdi - pointer to the pixel
sepia_asm:
    ; Save the alpha channel (or other metadata) of the pixel
    mov r8, [rdi + COLOR_SIZE*3]

    ; Convert each color component to float and load into xmm0, xmm1, xmm2
    ; Red
    movzx eax, byte [rdi]
    cvtsi2ss xmm0, eax
    shufps xmm0, xmm0, 0x00

    ; Green
    movzx eax, byte [rdi + COLOR_SIZE]
    cvtsi2ss xmm1, eax
    shufps xmm1, xmm1, 0x00

    ; Blue
    movzx eax, byte [rdi + COLOR_SIZE*2]
    cvtsi2ss xmm2, eax
    shufps xmm2, xmm2, 0x00

    ; Load the sepia color factors
    movups xmm3, [red_factors]
    movups xmm4, [green_factors]
    movups xmm5, [blue_factors]

    ; Apply sepia transformation
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5
    addps xmm0, xmm1
    addps xmm0, xmm2

    ; Convert back to integer and pack the result
    cvtps2dq xmm0, xmm0
    packusdw xmm0, xmm0
    packuswb xmm0, xmm0

    ; Store the result back
    movd [rdi], xmm0
    ; Restore the saved alpha channel (or other metadata)
    mov [rdi + COLOR_SIZE*3], r8

    ret
