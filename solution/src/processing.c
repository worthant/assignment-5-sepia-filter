#include "processing.h"
#include "image.h"
#include "utils.h"
#include <stdio.h>
#include <sys/time.h>

static double get_elapsed_time(struct timeval start, struct timeval end) {
    long seconds = end.tv_sec - start.tv_sec;
    long microseconds = end.tv_usec - start.tv_usec;
    return seconds + microseconds * 1e-6;
}

void process_image(const char* source, const char* destination, const char* filter_type) {
    struct timeval start, end;
    gettimeofday(&start, NULL);

    struct image img = read_image_from_file(source);
    enum FilterType filter = get_filter_type(filter_type);

    if (filter != FILTER_UNKNOWN) {
        apply_filter(filter, &img);
        write_image_to_file(destination, &img);
    } else {
        fprintf(stderr, "Invalid filter type. Use 'c' or 'sse'.\n");
    }

    destroy_img(&img);
    gettimeofday(&end, NULL);

    printf("Execution time: %.6f seconds.\n", get_elapsed_time(start, end));
}
