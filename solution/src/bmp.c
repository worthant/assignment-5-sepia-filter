#include "../include/bmp.h"
#include "../include/utils.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

static const uint16_t BMP_TYPE = 0x4D42; // 'BM' in little endian

/**
 * Calculate the row padding for a BMP file.
 * BMP rows are padded to ensure their size is a multiple of 4 bytes.
 *
 * @param width The width of the image in pixels.
 * @return The number of padding bytes.
 */
static size_t calculate_bmp_padding(uint32_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

/**
 * Reads a BMP image from a file.
 * This function reads a BMP file and converts it into the internal image format.
 * It supports only 24-bit BMP files without compression.
 *
 * @param in The input file stream.
 * @param img A pointer to the image structure where the data will be stored.
 * @return An enum indicating the read status.
 */
enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_TYPE) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != 24 || header.biCompression != 0) {
        return READ_INVALID_BITS;
    }

    *img = create_img(header.biWidth, header.biHeight);
    size_t padding = calculate_bmp_padding(header.biWidth);

    void* row = malloc(header.biWidth * sizeof(struct pixel) + padding);
    if (!row) {
        destroy_img(img);
        return READ_INVALID_HEADER;
    }

    for (uint32_t i = 0; i < header.biHeight; i++) {
        if (fread(row, header.biWidth * sizeof(struct pixel) + padding, 1, in) != 1) {
            free(row);
            destroy_img(img);
            return READ_INVALID_BITS;
        }
        copy_pixels(img->data + i * header.biWidth, row, header.biWidth);
    }

    free(row);
    return READ_OK;
}

/**
 * Writes an image to a file in BMP format.
 * This function writes the internal image format to a BMP file.
 * It supports writing only 24-bit BMP files without compression.
 *
 * @param out The output file stream.
 * @param img The image to be written.
 * @return An enum indicating the write status.
 */
enum write_status to_bmp(FILE* out, const struct image* img) {
    size_t padding = calculate_bmp_padding((uint32_t)img->width);
    size_t rowSize = img->width * sizeof(struct pixel) + padding;
    uint64_t imageSize = rowSize * img->height;
    uint64_t fileSize = sizeof(struct bmp_header) + imageSize;

    if (fileSize > UINT32_MAX || imageSize > UINT32_MAX) {
        fprintf(stderr, "Error: Image size exceeds UINT32_MAX limit.\n");
        return WRITE_ERROR;
    }

    struct bmp_header header = {
            // File Identification and Size
            .bfType = BMP_TYPE,                 // 'BM' for Bitmap
            .bfileSize = (uint32_t)fileSize,    // Total file size in bytes
            .bfReserved = 0,                    // Reserved; must be set to 0
            .bOffBits = sizeof(struct bmp_header), // Offset to start of image data

            // Image and Bitmap Information
            .biSize = 40,                       // BITMAPINFOHEADER size in bytes
            .biWidth = (uint32_t)img->width,    // Image width in pixels
            .biHeight = (uint32_t)img->height,  // Image height in pixels
            .biPlanes = 1,                      // Number of color planes
            .biBitCount = 24,                   // Bits per pixel

            // Compression and Resolution
            .biCompression = 0,                 // Compression type
            .biSizeImage = (uint32_t)imageSize, // Image size in bytes
            .biXPelsPerMeter = 0,               // Horizontal resolution
            .biYPelsPerMeter = 0,               // Vertical resolution

            // Color Information
            .biClrUsed = 0,                     // Colors in the color palette
            .biClrImportant = 0                 // Number of important colors
    };


    if (fwrite(&header, sizeof(header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    void* row = malloc(rowSize);
    if (!row) {
        return WRITE_ERROR;
    }

    for (uint32_t i = 0; i < img->height; i++) {
        copy_pixels(row, img->data + i * img->width, img->width);
        set_padding((uint8_t*)row + img->width * sizeof(struct pixel), padding);

        if (fwrite(row, rowSize, 1, out) != 1) {
            free(row);
            return WRITE_ERROR;
        }
    }

    free(row);
    return WRITE_OK;
}
